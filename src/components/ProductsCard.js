import { useState, useEffect } from 'react';
import AddProducts from '../pages/AddProducts'
import Admin from '../pages/AddProducts'
import Update from '../pages/Update';
import Archived from '../components/Archived';
import GetAllProducts from '../pages/GetAllProducts';
// import GetAllProductCard from '../components/GetAllProductCard';
import { Row , Col, Button, Table } from 'react-bootstrap';
import { Navigate, Link, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductsCard({productProp}){
	
	// console.log(productProp)
	
	// const [isActive, setIsActive] = useState('')
	// const { productId } = useParams();

	


	 const { name, description, product, price, isActive, _id } = productProp;
	 console.log(_id)

	function arch(productId) {
		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${_id}`,{
			method: "PUT",
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive:false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					title: "Disabled the product",
					icon: "success",
					
				})
				window.location.reload() 
			}else {
				Swal.fire({
					title: "Something Wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	function unArch(productId){
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${_id}`,{
			method: "PUT",
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive:true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					title: "Activate the product",
					icon: "success",
					
				})
				window.location.reload() 
			}else {
				Swal.fire({
					title: "Something Wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}
	// const [ products, setProducts ] =useState('')
	useEffect(() =>{
		
		
	},)

	return(
		<>
			

		<Row className="spacing" >
			<Col>
				<Table striped bordered hover className="table">
				      
				      <tbody className="admin">
				        <tr >
				          <td className="row1">{name}</td>
				          <td className="row1">{description}</td>
				          <td className="row1">{price} </td>
				      		
				          {
				          	isActive ?
				          	<td className="row1">Available</td>
				          	:
				          	<td className="row1">Not Available </td>
				          }
				          <td className="row1">
				          <Button className="btn btn-primary" as={Link} to={`/products/${_id}`}> Update 

				          </Button>

				          {
				          	isActive ?
				          	<Button onClick={()=> arch(_id)}className="btn btn-danger" >Disable
				          	 </Button>
				          	:
				          	<Button onClick={()=> unArch(_id)}className="btn btn-success" >Activate </Button>
				          }

				          {/*{
				          	isActive ?
				          	<Button className="btn btn-danger" as={Link} to={`/adminDashboard/archive/${_id}`} >Disable
				          	 </Button>
				          	:
				          	<Button className="btn btn-success" as={Link} to={`/adminDashboard/archive/${_id}`} >Activate </Button>
				          }*/}
				           
				          </td>
				        </tr>
				      </tbody>
				    </Table>
			</Col>
		</Row>
	</>
	)
}