import { useState, useEffect, useContext } from 'react';
import { Button, Row, Col, Table } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
// import AddProducts from '../pages/AddProducts';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Archive(productProp){

	const { productId } = useParams();

	const [ isActive, setIsActive ] =useState('')

	// const archive = () =>{
		
	// }

	// return (

	// 	)

	useEffect(()=>{

		
			 
			fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`,{
				method: "PUT",
				headers: {
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					isActive:false
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data=== true){
					Swal.fire({
						title: "Disabled the product",
						icon: "success",
						text: "Successfully Disabled The Product"
					})
				}else {
						Swal.fire({
							title: "Activate the product",
							icon: "success",
							text: "Successfully Activate The Product"
						})

				}
			})
			
				fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`,{
					method: "PUT",
					headers: {
						'Content-Type' : 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						isActive:true
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data === true){
						Swal.fire({
							title: "Activate the product",
							icon: "success",
							text: "Successfully Activate The Product"
						})
					}else {
						Swal.fire({
							title: "Disabled the product",
							icon: "success",
							text: "Successfully Disabled The Product"
						})
					}
				})
			

	})

	
}