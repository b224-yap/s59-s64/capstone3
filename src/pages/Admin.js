import { useEffect, useState } from 'react';
import ProductsCard from '../components/ProductsCard';
import { Table ,Row, Col, Button } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Products(){

	const [product, setProduct] = useState([]);
	const [isActive, setIsActive] = useState('');

	const{ productId } = useParams();


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`,{
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setProduct(data.map(product => {
				return (
					<ProductsCard key={product._id} productProp={product}/>
				)
			}))
		})
	},[])

	// useEffect(()=>{
		
		
	// },[])

	

	return(

		<>
		<h1> Admin Dashboard</h1>
		<Button className="btn btn-primary" as={Link} to="/addproducts">Add New Product
		</Button>
		<Button>Show User Orders 
		</Button>
		<h1>Products</h1>
		<Row>
			<Col> 
				<Table >
					<thead>
					  <tr className="mt-3 mb-3">
					    <th className="row1">Name</th>
					    <th className="row1">Description</th>
					    <th className="row1">Price</th>
					    <th className="row1">Availability</th>
					    <th className="row1">Actions</th>
					  </tr>
					</thead>
				</Table>
			</Col>
		</Row>
			
			
			{product}
			
		</>


	)
}

































