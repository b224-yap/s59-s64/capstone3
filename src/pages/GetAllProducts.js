import { useEffect, useState } from 'react';
// import ProductsCard from '../components/ProductsCard';
import GetAllProductCard from '../components/GetAllProductCard'
import { useParams } from 'react-router-dom';

export default function GetAllProducts(){

	const [products, setProducts] = useState([]);

	const{ productId } = useParams();


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allActiveProducts`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setProducts(data.map(product => {
				return (
					<GetAllProductCard key={product._id} productProp={product}/>
				)
			}))
		})
	},[])

	

	return(
		<>
			<h1>Products</h1>
			{products}
			
		</>


	)
}