import { useState, useEffect, useContext } from 'react';
import { useParams, Link, useNavigate} from 'react-router-dom';
import { Form , Button, Row, Col, Table } from 'react-bootstrap';
import ProductsCard from'../components/ProductsCard';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';



export default function AddProducts(){

	const [products, setProducts] = useState([])
	const navigate =useNavigate();

	const [ name, setName ]   =useState('');
	const [ description, setDescription ]   = useState('');
	const [ price, setPrice ]   = useState('');
	const { prodId } = useParams();

	const[isActive, setIsActive] = useState(false);

	function createProduct(prodId) {
		// e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`,{
			method:"POST",
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				// prodId: prodId
				name: name,
				description: description,
				price : price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true ){
				Swal.fire({
					title: "Add a Product",
					icon: "success",
					text: "Successfully added a product"
				})
				navigate("/adminDashboard")
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})

	}

	useEffect(()=> {
		console.log(prodId);
		fetch(`${process.env.REACT_APP_API_URL}/products/${prodId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
		})

		if(name !== null && description !== null && price !==null){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [prodId])

	return(

		<>

		{products}
		<Form onSubmit= {e => createProduct(e)}>

				<Form.Group className="mb-3" controlId="Name">
				  <Form.Label>Product Name</Form.Label>
				  <Form.Control 
				  	type="name" 
				  	placeholder="Enter Product Name"
				  	value={name}
				  	onChange={e => setName(e.target.value)}
				  	required
				  />
				</Form.Group>

				<Form.Group className="mb-3" controlId="description">
				  <Form.Label>Description</Form.Label>
				  <Form.Control 
				  	type="description" 
				  	placeholder="Description"
				  	value={description}
				  	onChange={e => setDescription(e.target.value)}
				  	required
				  />
				</Form.Group>

				<Form.Group className="mb-3" controlId="price">
				  <Form.Label>Price</Form.Label>
				  <Form.Control 
				  	type="price" 
				  	placeholder="Price"
				  	value={price}
				  	onChange={e => setPrice(e.target.value)}
				  	required
				  />
				</Form.Group>

				


		      {
		      	isActive ?
		      		<Button variant="primary" type="submit" id="submitBtn">
		      		  Submit
		      		</Button>
		      		:
		      		<Button variant="primary" type="submit" id="submitBtn" disabled>
		      		  Submit
		      		</Button>
		      }


		    </Form>



		    
		 </>
	)
}