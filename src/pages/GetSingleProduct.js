import { useState, useEffect, useContext } from 'react';
import {Container , Card , Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function GetSingleProduct(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();
	const { userId, productId, quantity } =useParams();

	const [ name , setName ] =useState("");
	const [ description, setDescription ] = useState("");
	const [ price, setPrice ] =useState(0);

	// const singleProduct =(productId) => {
	// 	console.log(productId)
	// 	fetch(`${process.env.REACT_APP_API_URL}/orders/addOrder`,{
	// 		method : "POST",
	// 		headers: {
	// 			'Content-Type' : 'application/json',
	// 			Authorization : `Bearer ${localStorage.getItem('token')}`
	// 		},
	// 		body: JSON.stringify({
	// 			userId : userId,
	// 			productId :productId,
	// 			quantity : quantity
	// 		})
	// 	})
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		if(data === true ){
	// 			Swal.fire({
	// 				title: "You created an order",
	// 				icon: "success",
	// 				text: "You have an Order"
	// 			})
	// 		} else {
	// 			Swal.fire({
	// 				title: "Something went wrong!",
	// 				icon: "error",
	// 				text: "Please try again"
	// 			})
	// 		}
	// 	})
	// };


	const singleProduct =(productId) => {
		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/orders/addOrder`,{
			method : "POST",
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({		
				productId :productId
	
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true ){
				Swal.fire({
					title: "You created an order",
					icon: "success",
					text: "You have an Order"
				})
			} else {
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	};


	useEffect(()=>{
		console.log(productId);
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
		})
	}, [productId])


	return(
		<Container>
					<Row className="mt-3 mb-3">
						<Col lg={{span: 6, offset:3}}>
							<Card>
							      <Card.Body className="text-center">
							        <Card.Title>{name}</Card.Title>
							        <h6>Description</h6>
							        <p>{description}</p>
							        <h6>Price:</h6>
							        <h6>PhP {price}</h6>
							       	

							         {
							     			(user.id !== null) ?
							        		<Button variant="primary" onClick={() => singleProduct(productId)} >Buy</Button>
							        		:
							        		<Button className="btn btn-danger" as={Link} to="/login"  >Log in to Buy Products</Button>
							    	}
							        

							      </Card.Body>
							    </Card>
						</Col>
					</Row>
				</Container>

	)
}