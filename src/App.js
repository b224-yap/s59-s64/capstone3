import './App.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import GetAllProductCard from './components/GetAllProductCard';
import Archived from './components/Archived';
import ProductsCard from './components/ProductsCard';
import AddProducts from './pages/AddProducts';

import GetSingleProduct from './pages/GetSingleProduct';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
// import Order from './pages/Order';
import GetAllProducts from './pages/GetAllProducts';
import Admin from './pages/Admin';
import Register from './pages/Register';
import Update from './pages/Update';
import { UserProvider } from './UserContext';

function App() {

    const [ user, setUser ] = useState({
      id:null,
      isAdmin:null
    });

    const unsetUser = () => {
      localStorage.clear();
    }


  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

        // User is logged in
        if(typeof data._id !== "undefined"){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
          // User is logged out
        } else {
          setUser({
            id:null,
            isAdmin: null
          })
        }
    })
  }, [])




  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
          <AppNavbar />
          <Container>
              <Routes>
                  <Route path="/archive" element={<ProductsCard/>} />
                  <Route path="/addproducts" element={<AddProducts/>} />
                  <Route path="adminDashboard/archive/:productId" element={<Archived/>} />
                  <Route path="/" element={<Home/>} />
                  <Route path="singleProduct/:productId" element={<GetSingleProduct/>} />
                  <Route path="/adminDashboard" element={<Admin/>} />
                  <Route path="/products/:productId" element={<Update/>} />
                  <Route path="/login" element={<Login/>} />
                  <Route path="/logout" element={<Logout/>} />
                  {/*<Route path="/order" element={<Order/>} />*/}
                  <Route path="/user/products" element={<GetAllProducts/>} />
                  

                  <Route path="/register" element={<Register/>} />
                  <Route path="/update" element={<Update/>} />
                  
              </Routes>
          </Container>
      </Router>
      </UserProvider>
     
  
    
  );
}

export default App;
